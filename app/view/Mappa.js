//var posizione = AttrazioniBambini.app.getController('Position').getPosition();
Ext.define('AttrazioniBambini.view.Mappa', {
  extend: 'Ext.Map',
  //xtype: 'mappa',
  alias: "widget.mappa",
  layout: {
    type: 'hbox',
    align: 'middle'
  },
  //posizione : AttrazioniBambini.app.getController('Position').getPosition(),
  config: {
   mapOptions: {
        center: new google.maps.LatLng(46.167943,8.869057),
        mapTypeId: google.maps.MapTypeId.HYBRID,
        zoom: 15
      },
      listeners : {
        maprender : function() {
          var marker = new google.maps.Marker({
            map: this.getMap(),
            position: new google.maps.LatLng (46.167943,8.869057),
            title : 'La tua Posizione'
          });
        }
      }
      
    },
});

