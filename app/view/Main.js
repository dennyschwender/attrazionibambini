tabpanel = Ext.define('AttrazioniBambini.view.Main', {
    extend: 'Ext.TabPanel',
    xtype: 'main',
    id:'mainPanel',
    config: {
        tabBarPosition: 'bottom',
        items: [{
            id:'panel1',
            xtype: 'mappa',
            iconCls: 'maps',
            title: 'Mappa'
        }, {
            xtype: 'titlebar',
            title: 'Slide Nav',
            docked: 'top',
            items: [{
                align: 'left',
                name: 'nav_btn',
                iconCls: 'list',
                ui: 'plain'
            }
            ]
        }
        ]
    },
    initialize: function () {

         this.getTabBar().hide();

    }
    
});

