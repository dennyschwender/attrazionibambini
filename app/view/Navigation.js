Ext.define('AttrazioniBambini.view.Navigation', {
	extend: 'Ext.List',
	xtype: 'navigation',
	requires : ['Ext.data.Store'],
	id:'list',
	config: {
		cls : 'nav-list',
		itemTpl : '{title}',
		data : [{
			title : 'Login',
			pagina: 'login'
		},{
			title : 'Mappa',
			pagina: 'mappa'
		}],
		listeners: {
             itemtap : function(list, index, target, record, event) {
                 var tabpanel = Ext.getCmp('mainPanel');
                 tabpanel.setActiveItem(
				    {
				        xtype: list._data[index].pagina,
			            iconCls: 'maps',
			            title: list._data[index].title
				    }
				);
             }
         }
	}
	
});