Ext.define('AttrazioniBambini.controller.MyController', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            signUpsignInCardPanel: 'signUpsignInCardPanel'
        },

        control: {
            "panel": {
                signUpSuccess: 'onSignUpsignInCardPanelSignUpSuccess',
                signInSuccess: 'onSignUpsignInCardPanelSignInSuccess'
            }
        }
    },

    onSignUpsignInCardPanelSignUpSuccess: function(userObject) {
        console.log(JSON.stringify(userObject));
        Ext.Msg.alert('signUpSuccess fired. This event should be honored in main application');
    },

    onSignUpsignInCardPanelSignInSuccess: function(userObject) {
        console.log(JSON.stringify(userObject));
        Ext.Msg.alert('signUpSuccess fired. This event should be honored in main application');
        var signUpsignInCardPanel = this.getSignUpsignInCardPanel();

    }

});