var pos=5;
Ext.define('AttrazioniBambini.controller.Position', {
    extend: 'Ext.app.Controller',

    getPosition: function() {
        //debugger;        
        Ext.device.Geolocation.getCurrentPosition({
            allowHighAccuracy: true,
            timeout: 500,
            success: function(position) {
                console.log(position.coords);
               pos=position.coords.latitude;
               Ext.Msg.alert(position.coords);
            },
            failure: function() {
                console.log('something went wrong!');
                Ext.Msg.alert("something");
            }
        });
        return pos;
    }
});