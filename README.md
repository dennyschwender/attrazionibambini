Attrazioni bambini
========

Applicativo nel quale le mamme (che si sono registrate nel sistema) potranno inserire nuove attrazioni per bambini nel canton Ticino.


Descrizione
---------------
In questo progetto si dovr� creare un applicativo nel quale le mamme (che si sono registrate nel sistema) potranno inserire nuove attrazioni per bambini nel canton Ticino.
Dovranno poter scegliere il luogo dell�attrazione utilizzando una mappa google (se si trovano sul posto il luogo dovr� venir impostato automaticamente utilizzando la geolocalizzazione), di cosa si tratta (descrizione), per quali et� � consigliata, eventuali costi, foto, orari di apertura ed osservazioni.
Naturalmente le attrazioni potranno essere raggruppate in categorie, le quali devono essere gestite a loro volta (dagli utenti registrati).
Naturalmente poi queste attrazioni potranno essere consultate genericamente (guardando la mappa) oppure filtrate attraverso vari criteri di ricerca (ad esempio distretto del Ticino se fattibile, distanza, costo da..a, et�, attrazioni con migliori punteggi �).
Si dovr� poter impostare anche il percorso dal luogo attuale all�attrazione desiderata, con durata, chilometri ecc.
Vi dovr� essere anche una sezione commenti nella quale gli utenti registrati potranno descrivere la propria esperienza assegnando anche un punteggio.
Vi dovranno essere delle statistiche in grado di definire, in base ai punteggi degli utenti, una graduatoria.
Chi non � registrato potr� accedere alla mappa, effettuare le ricerche ma non commentare n� inserire nuovi siti.
Il database principale risieder� su un server esterno, ma sarebbe opportuno disporre localmente dei dati in caso di assenza di connessione. Bisogner� quindi predisporre degli aggiornamenti del database a scadenze regolari (che l�utente potr� impostare).
